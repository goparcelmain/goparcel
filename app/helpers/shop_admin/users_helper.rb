module ShopAdmin
  module UsersHelper
    def current_user
      current_shop_admin_shop_user
    end

    def user_signed_in?
      shop_admin_shop_user_signed_in?
    end
  end
end
