class Store < ActiveRecord::Base
  belongs_to :shop

  def address
    "#{postal_code}, #{city}, улица #{street}, дом #{house}"
  end

  def address_info
    "здание #{building}, строение #{housing}, комната #{room}, подъезд #{porch}, этаж #{floor}"
  end

  def title_with_shop
    "Магазин #{shop.title}, склад #{title}"
  end
end
