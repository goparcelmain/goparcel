class LegalEntity < ActiveRecord::Base
  belongs_to :legal, polymorphic: true
  belongs_to :user, polymorphic: true
end
