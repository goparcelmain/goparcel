class ShopUser < ActiveRecord::Base
  has_many :shops
  has_many :stores, through: :shops
  has_many :orders
  has_one :legal_entity, as: :user
  has_many :notifications, as: :user
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  
  before_create :build_default_relations

  private

    def build_default_relations
      build_legal_entity
    end
end
