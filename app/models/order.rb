class Order < ActiveRecord::Base
  has_many :order_items
  belongs_to :shop_user
  belongs_to :store
  belongs_to :shop

  STATUSES = {
      unpublished: 0,
      published: 1,
      confirmed: 2,
      assigned: 3,
      courier_canceled: 4,
      shop_canceled: 5,
      courier: 6,
      courier_returned: 7,
      shop_returned: 8,
      delivered: 9,
      returned: 10
    }

  PAYMENT_TYPES = %w(other cash card)

  RETURN_REASONS = %w(rejected_by_customer courier_failed)
  RETURN_METHODS = %w(direct_to_shop through_courier_store)

  validates :status_id,
    {
      numericality: {
          only_integer: true,
          greater_than: -1,
          less_than: 11
        }
    }
  
  def has_status?(status_name)
    return false unless STATUSES.has_key? status_name
    return false unless status_id == STATUSES[status_name]
    true 
  end

  def update_status(status_name, attributes = {})
    if update_attributes(attributes.merge(status_id: STATUSES[status_name]))
      shop_user.notifications.create({
          subject: I18n.t('notifications.subject.order_status_changed', id: id),
          message: I18n.t("notifications.body.status_changed_to_#{status_name}", id: id)
        })
      true
    else
      false
    end
  end

  def status_options_list
    STATUSES.each_pair{ |k, v| [I18n.t("order.status.#{k}"), v] }
  end

  def payment_types_options_list
    PAYMENT_TYPES.map{ |v| [I18n.t("order.payment_type.#{v}"), v] }
  end

  def status_name
    I18n.t("order.status.#{STATUSES.to_a[status_id].first}")
  end

  def payment_type_name
    I18n.t("order.payment_type.#{payment_type}")
  end
end
