class Shop < ActiveRecord::Base
  belongs_to :shop_user
  has_many :stores
  has_many :shop_delivery_values

  def calculate_compensation(sum)
    condition = 'less_than < :sum OR greater_than > :sum OR equal_to = :sum'
    shop_delivery_value = shop_delivery_values.where(condition, sum: sum)
      .order(delivery_value: :asc).first
    if shop_delivery_value
      shop_delivery_value.delivery_value
    else
      0
    end
  end
end
