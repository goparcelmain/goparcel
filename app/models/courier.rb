class Courier < ActiveRecord::Base
  STATUSES = %w(active inactive)

  def self.active
    where(status_id: 0)
  end
end
