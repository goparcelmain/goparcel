class CourierUser < ActiveRecord::Base
  has_many :orders
  has_one :courier_service
  has_one :legal_entity, as: :user
  has_many :couriers

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  
  before_create :build_default_relations

  private

    def build_default_relations
      build_courier_service
      build_legal_entity
    end
end
