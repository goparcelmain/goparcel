class Notification < ActiveRecord::Base
  belongs_to :user, polymorphic: true

  default_scope { order(status: :asc) }
end
