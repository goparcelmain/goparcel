class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def after_sign_in_path_for(resource)
    user_home_path resource
  end

  def after_sign_out_path_for(scope)
    if scope == :shop_admin_shop_user
      shop_admin_root_path
    else
      super
    end
  end

  private
    def user_home_path(user)
      if user.class == ShopUser
        shop_admin_root_path
      elsif user.class == CourierUser
        courier_admin_root_path
      else
        root_path
      end
    end
end
