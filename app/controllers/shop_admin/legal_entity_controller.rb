module ShopAdmin
  class LegalEntityController < ShopAdmin::ApplicationController
    before_action :set_legal_entity, only: [:show, :edit, :update, :destroy]

    def show
    end

    def edit
    end

    def update
      if @legal_entity.update(legal_entity_params)
        redirect_to edit_shop_admin_legal_entity_path, notice: 'Legal entity was successfully updated.'
      else
        render :edit
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_legal_entity
        @legal_entity = current_user.legal_entity
      end

      def legal_entity_params 
        params.require(:legal_entity).permit(:title, :legal_address, :physical_address,
          :ogrn, :inn, :kpp, :okved, :okpo, :bik, :bank_name,
          :correspondent_account,:checking_account, :general_manager,
          :general_accountant, :contact_phone, :contact_email)
      end
  end
end
