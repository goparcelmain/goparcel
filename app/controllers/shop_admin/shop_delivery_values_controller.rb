module ShopAdmin
  class ShopDeliveryValuesController < ShopAdmin::ApplicationController
    before_action :set_shop
    before_action :set_shop_delivery_value, except: [:new, :create]
    
    # GET /shop_delivery_values/1
    def show
    end

    # GET /shop_delivery_values/new
    def new
      @shop_delivery_value = ShopDeliveryValue.new
    end

    # GET /shop_delivery_values/1/edit
    def edit
    end

    # POST /shop_delivery_values
    def create
      @shop_delivery_value = @shop.shop_delivery_values.build(shop_delivery_value_params)

      if @shop_delivery_value.save
        redirect_to [:shop_admin, @shop], notice: 'Shop delivery value was successfully created.'
      else
        render :new
      end
    end

    # PATCH/PUT /shop_delivery_values/1
    def update
      if @shop_delivery_value.update(shop_delivery_value_params)
        redirect_to [:shop_admin, @shop], notice: 'Shop delivery value was successfully updated.'
      else
        render :edit
      end
    end

    # DELETE /shop_delivery_values/1
    def destroy
      @shop_delivery_value.destroy
      redirect_to shop_admin_shop_url(@shop), notice: 'Shop delivery value was successfully destroyed.'
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_shop_delivery_value
        @shop_delivery_value = @shop.shop_delivery_values.find(params[:id])
      end

      def set_shop
        @shop = current_user.shops.find(params[:shop_id])
      end

      # Only allow a trusted parameter "white list" through.
      def shop_delivery_value_params
        params.require(:shop_delivery_value).permit(:less_than, :greater_than,
            :equal_to, :shop_delivery_value)
      end
  end
end
