module ShopAdmin
  class IndexController < ShopAdmin::ApplicationController
    def index
    	redirect_to shop_admin_profile_path if user_signed_in?
    end
  end
end
