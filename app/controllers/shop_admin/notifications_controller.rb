module ShopAdmin
  class NotificationsController < ShopAdmin::ApplicationController
    before_action :set_notification, only: [:destroy]

    def index
      @notifications = current_user.notifications.all
    end

    def destroy
      @notification.destroy
      redirect_to shop_admin_notifications_url, notice: 'Notification was successfully destroyed.'
    end

    private
      
      def set_notification
        @notification = current_user.notifications.find(params[:id])
      end
  end
end
