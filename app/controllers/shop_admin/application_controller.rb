module ShopAdmin
  class ApplicationController < ActionController::Base
    before_action :authenticate_shop_admin_shop_user!
    protect_from_forgery with: :exception
    layout 'shop_admin'
    helper ShopAdmin::UsersHelper
    include ShopAdmin::UsersHelper
  end
end