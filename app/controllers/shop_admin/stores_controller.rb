module ShopAdmin
  class StoresController < ShopAdmin::ApplicationController
    before_action :set_store, except: [:index, :new, :create]

    def index
      @stores = current_user.stores
    end

    # GET /stores/1
    def show
    end

    # GET /stores/new
    def new
      @store = Store.new
    end

    # GET /stores/1/edit
    def edit
    end

    # POST /stores
    def create
      @store = Store.new(store_params)

      if @store.save
        redirect_to [:shop_admin, @store], notice: 'Store was successfully created.'
      else
        render :new
      end
    end

    # PATCH/PUT /stores/1
    def update
      if @store.update(store_params)
        redirect_to [:shop_admin,  @store], notice: 'Store was successfully updated.'
      else
        render :edit
      end
    end

    # DELETE /stores/1
    def destroy
      @store.destroy
      redirect_to shop_admin_stores_url, notice: 'Store was successfully destroyed.'
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_store
        @store = current_user.stores.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def store_params
        params.require(:store).permit(:shop_id, :title, :contact_name, :postal_code,
          :city, :street, :house, :building, :housing, :room,
          :porch, :floor, :phone, :description)
      end
  end
end
