module ShopAdmin
  class OrdersController < ShopAdmin::ApplicationController
    before_action :set_order, except: [:index, :new, :create]

    # GET /orders
    def index
      @q = current_user.orders.search(params[:q])
      @orders = @q.result
    end

    # GET /orders/1
    def show
    end

    # GET /orders/new
    def new
      @order = Order.new
    end

    # GET /orders/1/edit
    def edit
    end

    # POST /orders
    def create
      @order = current_user.orders.build(order_params)
      if @order.save
        redirect_to [:shop_admin, @order], notice: 'Order was successfully created.'
      else
        render :new
      end
    end

    # PATCH/PUT /orders/1
    def update
      if @order.update(order_params)
        redirect_to [:shop_admin, @order], notice: 'Order was successfully updated.'
      else
        render :edit
      end
    end

    def publish
      if OrderStatusManager.new(@order).publish
        flash[:notice] = 'Order was successfully published.'
      end
      redirect_to [:shop_admin, @order]
    end

    def confirm
      if OrderStatusManager.new(@order).confirm
        flash[:notice] = 'Order was successfully confirmed.'
      end
      redirect_to [:shop_admin, @order]
    end

    def cancel
      if OrderStatusManager.new(@order).cancel_by_shop
        flash[:notice] = 'Order was successfully canceled.'
      end
      redirect_to [:shop_admin, @order]
    end

    def courier
      if OrderStatusManager.new(@order).transfer_to_courier
        flash[:notice] = 'Order was successfully transfered to courier.'
      end
      redirect_to [:shop_admin, @order]
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_order
        @order = current_user.orders.find(params[:id])
      end

      def order_params
        store = current_user.stores.find(params[:order][:store_id])

        params.require(:order).permit(:shop_user_id, :shop_title,
          :shop_order_id, :shop_price, :store_id, :store_address, :store_address_info,
          :courier_order_id,:courier_id, :courier_order_id, :status_id,
          :customer_name, :customer_phone, :customer_second_phone, :customer_email,
          :delivery_address, :goods_estimated_price, :goods_price, :intercom_code, :payment_type, :comments).merge({
            shop_user_id: current_user.id,
            shop_id: store.shop.id,
            shop_title: store.shop.title,
            shop_price: store.shop.calculate_compensation(params[:order][:goods_price]),
            store_address: store.address,
            store_address_info: store.address_info
          })
      end

      def search_params
        params.require(:order).permit(:shop_id, :shop_order_id, :created_at, :status_id, :store_id)
      end
  end
end
