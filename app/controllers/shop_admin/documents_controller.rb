module ShopAdmin
  class DocumentsController < ShopAdmin::ApplicationController
    def transmission_reception
    end

    def transmission_return
    end

    def agent
    end

    def cancel
    end
  end
end
