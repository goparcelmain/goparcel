module ShopAdmin
  class OrderItemsController < ShopAdmin::ApplicationController
    before_action :set_order
    before_action :set_order_item,
        only: [:show, :edit, :update, :destroy]

    # GET /order_items/new
    def new
      @order_item = OrderItem.new
    end

    # GET /order_items/1/edit
    def edit
    end

    # POST /order_items
    def create
      @order_item = @order.order_items.build(order_item_params)

      if @order_item.save
        redirect_to [:shop_admin, @order], notice: 'Order item was successfully created.'
      else
        render :new
      end
    end

    # PATCH/PUT /order_items/1
    def update
      if @order_item.update(order_item_params)
        redirect_to [:shop_admin, @order], notice: 'Order item was successfully updated.'
      else
        render :edit
      end
    end

    # DELETE /order_items/1
    def destroy
      @order_item.destroy
      redirect_to shop_admin_order_url(@order), notice: 'Order item was successfully destroyed.'
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_order
        @order = current_user.orders.find(params[:order_id])
      end

      def set_order_item
        @order_item = @order.order_items.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def order_item_params
        params.require(:order_item).permit(:shop_good_id,
          :size, :price, :estimated_price, :count)
      end
  end
end
