module ShopAdmin
  class AccountSettingsController < ShopAdmin::ApplicationController
    before_action :set_user

    def edit
    end

    def update
      if @user.update_with_password(user_params)
      else
        render :edit
      end
    end

    private
      def set_user
        @user = current_user
      end

      def user_params
        params.require(:shop_user).permit(:email, :password, :password_confirmation, :current_password)
      end
  end
end