module GpCore
  module Api
    module V1
      class OrdersController < ApiApplicationController
        def create
          order = Order.create(published_order_params)
          respond_with(order)
        end

        private
          def published_order_params
            params[:order].merge!(status_id: 0,
              delivery_value: 500,
              goods_estimated_value: 1000)
            
            params.require(:order).permit(:shop_user_id, :shop_id, :shop_order_id, :store_id,
              :store_address, :courier_order_id, :courier_id, :courier_order_id,
              :status_id, :client_name, :client_phone, :client_second_phone,
              :client_email, :address, :floor, :intercom_code, :goods_title,
              :goods_count, :goods_weight, :goods_estimated_value, :goods_value,
              :payment_type, :comments)
          end
      end
    end
  end
end
