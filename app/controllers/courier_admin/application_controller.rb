module CourierAdmin
  class ApplicationController < ActionController::Base
    before_action :authenticate_courier_user!
    protect_from_forgery with: :exception
    layout 'courier_admin'
  end
end