module CourierAdmin
  class LegalEntitiesController < CourierAdmin::ApplicationController
    before_action :set_legal_entity, only: [:show, :edit, :update, :destroy]

    # GET /legal_entities
    def index
      @legal_entities = current_courier_user.legal_entities.all
    end

    # GET /legal_entities/1
    def show
    end

    # GET /legal_entities/new
    def new
      @legal_entity = LegalEntity.new
    end

    # GET /legal_entities/1/edit
    def edit
    end

    # POST /legal_entities
    def create
      @legal_entity = current_courier_user.legal_entities.build(legal_entity_params)

      if @legal_entity.save
        redirect_to [:courier_admin, @legal_entity], notice: 'Legal entity was successfully created.'
      else
        render :new
      end
    end

    # PATCH/PUT /legal_entities/1
    def update
      if @legal_entity.update(legal_entity_params)
        redirect_to [:courier_admin, @legal_entity], notice: 'Legal entity was successfully updated.'
      else
        render :edit
      end
    end

    # DELETE /legal_entities/1
    def destroy
      @legal_entity.destroy
      redirect_to courier_admin_legal_entities_url, notice: 'Legal entity was successfully destroyed.'
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_legal_entity
        @legal_entity = current_courier_user.legal_entities.find(params[:id])
      end

      def legal_entity_params 
        params.require(:legal_entity).permit(:title, :legal_address, :physical_address,
          :ogrn, :inn, :kpp, :okved, :okpo, :bik, :bank_name,
          :correspondent_account,:checking_account, :general_manager,
          :general_accountant, :contact_phone, :contact_email)
      end
  end
end
