module CourierAdmin
  class CourierServicesController < CourierAdmin::ApplicationController
    before_action :set_courier_admin_courier_service, only: [:show, :edit, :update, :destroy]

    # GET /courier_admin/courier_services
    def index
      @courier_services = current_courier_user.courier_services.all
    end

    # GET /courier_admin/courier_services/1
    def show
    end

    # GET /courier_admin/courier_services/new
    def new
      @courier_service = CourierService.new
    end

    # GET /courier_admin/courier_services/1/edit
    def edit
    end

    # POST /courier_admin/courier_services
    def create
      @courier_service = current_courier_user.courier_services.new(courier_service_params)

      if @courier_service.save
        redirect_to [:courier_admin, @courier_service], notice: 'Courier service was successfully created.'
      else
        render :new
      end
    end

    # PATCH/PUT /courier_admin/courier_services/1
    def update
      if @courier_service.update(courier_service_params)
        redirect_to [:courier_admin, @courier_service], notice: 'Courier service was successfully updated.'
      else
        render :edit
      end
    end

    # DELETE /courier_admin/courier_services/1
    def destroy
      @courier_service.destroy
      redirect_to courier_admin_courier_services_url, notice: 'Courier service was successfully destroyed.'
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_courier_admin_courier_service
        @courier_service =  current_courier_user.courier_services.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def courier_service_params
        params.require(:courier_service).permit(:title, :contact_name,
          :address, :store_address, :store_contact_name, :phone,
          :store_phone, :email)
      end
  end
end
