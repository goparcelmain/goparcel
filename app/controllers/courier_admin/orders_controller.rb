module CourierAdmin
  class OrdersController < CourierAdmin::ApplicationController
    before_action :set_order, except: [:index, :new, :create]

    # GET /orders
    def index
      @orders = current_courier_user.orders.all
    end

    # GET /orders/1
    def show
    end

    # GET /orders/new
    def new
      @order = Order.new
    end

    # GET /orders/1/edit
    def edit
    end

    # POST /orders
    def create
      @order = current_courier_user.orders.build(order_params)

      if @order.save
        redirect_to [:courier_admin, @order], notice: 'Order was successfully created.'
      else
        render :new
      end
    end

    # PATCH/PUT /orders/1
    def update
      if @order.update(order_params)
        redirect_to [:courier_admin, @order], notice: 'Order was successfully updated.'
      else
        render :edit
      end
    end

    # DELETE /orders/1
    def destroy
      @order.destroy
      redirect_to courier_admin_orders_url, notice: 'Order was successfully destroyed.'
    end

    def courier
      if OrderStatusManager.new(@order).transfer_to_courier
        flash[:notice] = 'Order was successfully transfered to courier.'
      end
      redirect_to [:courier_admin, @order]
    end

    def cancel
      if OrderStatusManager.new(@order).cancel_by_courier
        flash[:notice] = 'Order was successfully canceled.'
      end
      redirect_to [:courier_admin, @order]
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_order
        @order = Order.where(id: params[:id],
          courier_user_id: current_courier_user.id).first
      end

      def order_params
        
        params.require(:order).permit(:status_id, :courier_user_id, :shop_id,
          :shop_order_id, :store_id, :store_address, :courier_order_id,
          :courier_id, :courier_order_id, :status_id, :client_name, :client_phone,
          :client_second_phone, :client_email, :address, :floor, :intercom_code,
          :goods_title, :goods_count, :goods_weight, :goods_estimated_value,
          :goods_value, :delivery_value, :goods_estimated_value, :payment_type,
          :comments)
      end
  end
end
