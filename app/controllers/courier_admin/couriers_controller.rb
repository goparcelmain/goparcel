module CourierAdmin
  class CouriersController < CourierAdmin::ApplicationController
    before_action :set_courier_admin_courier, only: [:show, :edit, :update, :destroy]

    # GET /courier_admin/couriers
    def index
      @couriers = current_courier_user.couriers.all
    end

    # GET /courier_admin/couriers/1
    def show
    end

    # GET /courier_admin/couriers/new
    def new
      @courier = Courier.new
    end

    # GET /courier_admin/couriers/1/edit
    def edit
    end

    # POST /courier_admin/couriers
    def create
      @courier = current_courier_user.couriers.new(courier_params)

      if @courier.save
        redirect_to [:courier_admin, @courier], notice: 'Courier was successfully created.'
      else
        render :new
      end
    end

    # PATCH/PUT /courier_admin/couriers/1
    def update
      if @courier.update(courier_admin_courier_params)
        redirect_to [:courier_admin, @courier], notice: 'Courier was successfully updated.'
      else
        render :edit
      end
    end

    # DELETE /courier_admin/couriers/1
    def destroy
      @courier.destroy
      redirect_to courier_admin_couriers_url, notice: 'Courier was successfully destroyed.'
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_courier_admin_courier
        @courier = current_courier_user.couriers.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def courier_params
        params.require(:courier)
          .permit(:name, :status_id, :passport, :transport_type, :phone,
            :payments, :string, :orders_type)
      end
  end
end
