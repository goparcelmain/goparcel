class ApiApplicationController < ActionController::Base
  respond_to :json
  self.responder = ApiResponder
end
