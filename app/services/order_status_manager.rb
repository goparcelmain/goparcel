class OrderStatusManager
  def initialize(order)
    @order = order
  end

  def assign_to_active_courier_or_service
    courier = Courier.active.first
    return assign_courier(courier) unless courier.nil?

    service = CourierService.active.first
    return assign_courier_service(service) unless service.nil?

    return false
  end

  def assign_courier_service(courier_service)
    return false unless @order.has_status?(:confirmed)

    @order.update_status(:assigned, {
        courier_user_id: courier_service.courier_user_id,
        assigned_at: DateTime.now
      })
  end

  def assign_courier(courier)
    return false unless @order.has_status?(:confirmed)

    @order.update_status(:assigned, {
        courier_id: courier.id,
        courier_name: courier.name,
        courier_user_id: courier.courier_user_id,
        assigned_at: DateTime.now
      })
  end

  def publish
    @order.update_status(:published)
  end

  def cancel_by_shop
    @order.update_status(:shop_canceled)
  end

  def cancel_by_courier
    @order.update_status(:courier_canceled)
  end

  def confirm
    return false unless @order.has_status?(:published)
    @order.update_status(:confirmed)
    assign_to_active_courier_or_service
  end

  def transfer_to_courier
    return false unless @order.has_status?(:assigned)
    @order.update_status(:courier, shipped_at: DateTime.now)
  end

  def courier_return
    return false unless @order.has_status?(:courier)
    @order.update_status(:courier_returned)
  end

  def shop_return
    return false unless @order.has_status?(:courier)
    @order.update_status(:shop_returned)
  end

  def delivered
    return false unless @order.has_status?(:courier)
    @order.update_status(:delivered, delivered_at: DateTime.now)
  end

  def returned
   @order.update_status(:returned)
  end
end
