module Features
  module ShopAdmin
    module SessionHelpers
      def shop_admin_sign_up_with(email, password)
        visit new_shop_user_registration_path
        fill_in 'Email', with: email
        fill_in 'Password', with: password
        click_button 'Sign up'
      end

      def shop_admin_sign_in_with(email, password)
        visit new_shop_user_session_path
        fill_in 'Email', with: email
        fill_in 'Password', with: password
        click_button 'Log in'
      end

      def shop_admin_user
        @shop_admin_user ||= FactoryGirl.create(:shop_user, email: 'user@example.com', password: 'password123')
      end

      def visit_shop_admin_as_signed_in_user
        shop_admin_sign_in_with(shop_admin_user.email, shop_admin_user.password)
        visit shop_admin_root_path
      end
    end
  end
end