require 'rails_helper'

feature "Orders management" do
  let!(:courier_user) { FactoryGirl.create(:courier_user, email: 'courier@example.com', password: 'password123') }
  let!(:courier_service) { FactoryGirl.create(:courier_service, courier_user: courier_user) }
  let!(:shop) { FactoryGirl.create(:shop, shop_user: shop_admin_user) }
  let!(:store) { FactoryGirl.create(:store, shop: shop) }
  let!(:order) { FactoryGirl.create(:order, store: store, shop_user: shop_admin_user) }
  let!(:order_with_item) { FactoryGirl.create(:order, store: store, shop_user: shop_admin_user) }
  let!(:order_item) { FactoryGirl.create(:order_item, order: order_with_item) }
  
  before :each do
    visit_shop_admin_as_signed_in_user

    click_link 'Заказы'
  end

  scenario "show orders list" do
    expect(current_path).to eq(shop_admin_orders_path)
    expect(page).to have_content('Заявки')
    expect(page).to have_content(order.id)
  end

  scenario "show order" do
    within "tr[data-order-id='#{order.id}']" do
      click_link 'Перейти'
    end

    expect(current_path).to eq(shop_admin_order_path(order))
    expect(page).to have_content(order.id)
  end

  scenario "create order" do
    click_link "Добавить"

    fill_in 'Shop order', with: '123'
    fill_in 'Goods price', with: '100'
    click_button 'Создать'
 
    expect(current_path).to eq(shop_admin_order_path(shop_admin_user.orders.last))
    expect(page).to have_content('created')
  end

  scenario "edit order" do
    new_order_id = "dadsdasdasdasd"

    within "tr[data-order-id='#{order.id}']" do
      click_link 'Редактировать'
    end

    expect(current_path).to eq(edit_shop_admin_order_path(order))
    
    fill_in 'Shop order', with: new_order_id
    click_button 'Сохранить'

    expect(page).to have_content('updated')
    expect(page).to have_content(new_order_id)
  end

  scenario "add item" do
    within "tr[data-order-id='#{order.id}']" do
      click_link 'Перейти'
    end

    click_link 'Добавить товар'

    fill_in 'Shop good', with: '1234567'
    fill_in 'Size', with: '10x10x10'
    fill_in 'Price', with: '10'
    fill_in 'Estimated price', with: '5'
    fill_in 'Count', with: '3'
    click_button 'Создать'

    expect(current_path).to eq(shop_admin_order_path(order))
    expect(page).to have_content('created')
    expect(page).to have_css("tr[data-order-item-id='#{order.order_items.last.id}']")
  end

  scenario "publish/confirm/cancel/courier order" do
    within "tr[data-order-id='#{order_with_item.id}']" do
      click_link 'Перейти'
    end

    click_link 'Опубликовать'
    expect(page).to have_content('published')
  end

  scenario "confirm order" do
    within "tr[data-order-id='#{order_with_item.id}']" do
      click_link 'Перейти'
    end

    click_link 'Опубликовать'
    click_link 'Подтвердить'

    expect(page).to have_content('confirmed')
  end

  scenario "cancel order" do
    within "tr[data-order-id='#{order_with_item.id}']" do
      click_link 'Перейти'
    end

    click_link 'Опубликовать'
    click_link 'Отменить'

    expect(page).to have_content('canceled')
  end

  scenario "courier order" do
    within "tr[data-order-id='#{order_with_item.id}']" do
      click_link 'Перейти'
    end

    click_link 'Опубликовать'
    click_link 'Подтвердить'
    click_link 'Передать курьеру'
    
    expect(page).to have_content('transfered to courier')
  end
end
