require 'rails_helper'

feature "User shops" do
  let!(:shops) { FactoryGirl.create_list(:shop, 5, shop_user: shop_admin_user) }

  before :each do
    visit_shop_admin_as_signed_in_user

    within '.profile-navigation' do
      click_link 'Магазины'
    end
  end

  scenario "show list" do    
    shops.each do |shop|
      expect(page).to have_content(shop.title)
    end

    expect(page).to have_link('Добавить')
  end


  scenario "show shop" do
    click_link shops.first.title

    expect(current_path).to eq(shop_admin_shop_path(shops.first.id))
    expect(page).to have_content(shops.first.title)
  end

  scenario "add new shop" do
    shop_title = 'Test shop title'

    click_link 'Добавить'

    fill_in 'Title', with: shop_title
    click_button 'Создать'

    expect(current_path).to eq(shop_admin_shop_path(shop_admin_user.shops.last.id))
    expect(page).to have_content(shop_title)
  end

  scenario "update shop info" do
    old_title = shops.first.title
    new_title = 'Test shop title 123'

    within "tr[data-shop-id='#{shops.first.id}']" do
      click_link 'Редактировать'
    end

    fill_in 'Title', with: new_title
    click_button 'Сохранить'

    expect(current_path).to eq(shop_admin_shop_path(shops.first.id))
    expect(page).to have_content(new_title)
    expect(page).not_to have_content(old_title)
  end

  scenario "delete shop" do
    deleted_title = shops.first.title

    within "tr[data-shop-id='#{shops.first.id}']" do
      click_link 'Удалить'
    end

    expect(current_path).to eq(shop_admin_shops_path)
    expect(page).not_to have_content(deleted_title)
  end
end
