require 'rails_helper'

feature 'User dashboard' do
  let!(:shops) { FactoryGirl.create_list(:shop, 5, shop_user: shop_admin_user) }
  let!(:stores) { FactoryGirl.create_list(:store, 5, shop: shops.first) }
  let!(:notifications) { FactoryGirl.create_list(:notification, 5, user: shop_admin_user)}
  before :each do
    visit_shop_admin_as_signed_in_user
  end

  scenario 'redirect signed in user to dashboard' do
    expect(current_path).to eq(shop_admin_profile_path)
  end

  scenario 'show shops and add button' do
    expect(page).to have_content(shops[0].title)
    expect(page).to have_content(shops[1].title)
    expect(page).to have_content(shops[2].title)

    expect(page).not_to have_content(shops[3].title)
    expect(page).not_to have_content(shops[4].title)

    expect(page).to have_link('Добавить магазин')
  end

  scenario 'show stores and add button' do
    expect(page).to have_content(stores[0].title)
    expect(page).to have_content(stores[1].title)
    expect(page).to have_content(stores[2].title)

    expect(page).not_to have_content(stores[3].title)
    expect(page).not_to have_content(stores[4].title)

    expect(page).to have_link('Добавить склад')
  end

  scenario 'show notifications' do
    notifications.each do |notification|
      expect(page).to have_content(notification.subject)
      expect(page).to have_content(notification.message)
    end
  end
end
