require 'rails_helper'

feature "Visitor sign in" do
  before :each do
    FactoryGirl.create(:shop_user, email: 'user@example.com', password: 'password123')
  end

  scenario "with valid credentials" do
    shop_admin_sign_in_with 'user@example.com', 'password123'
    expect(page).to have_content 'user@example.com'
  end
end
