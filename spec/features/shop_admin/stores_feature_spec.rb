require 'rails_helper'

feature "User storehouses" do
  let!(:shop) { FactoryGirl.create(:shop, shop_user: shop_admin_user) }
  let!(:stores) { FactoryGirl.create_list(:store, 5, shop: shop) }

  before :each do
    visit_shop_admin_as_signed_in_user

    within '.profile-navigation' do
      click_link 'Склады'
    end
  end

  scenario "show list" do    
    stores.each do |store|
      expect(page).to have_content(store.title)
    end

    expect(page).to have_link('Добавить')
  end

  scenario "show store" do
    click_link stores.first.title

    expect(current_path).to eq(shop_admin_store_path(stores.first.id))
    expect(page).to have_content(stores.first.title)
  end

  scenario "add new store" do
    store_title = 'Test store title'

    click_link 'Добавить'

    fill_in 'Title', with: store_title
    click_button 'Создать'

    expect(current_path).to eq(shop_admin_store_path(shop.stores.last.id))
    expect(page).to have_content(store_title)
  end

  scenario "update store info" do
    old_title = stores.first.title
    new_title = 'Test store title 123'

    within "tr[data-store-id='#{stores.first.id}']" do
      click_link 'Редактировать'
    end

    fill_in 'Title', with: new_title
    click_button 'Сохранить'

    expect(current_path).to eq(shop_admin_store_path(stores.first.id))
    expect(page).to have_content(new_title)
    expect(page).not_to have_content(old_title)
  end

  scenario "delete store" do
    deleted_title = stores.first.title

    within "tr[data-store-id='#{shop.stores.first.id}']" do
      click_link 'Удалить'
    end

    expect(current_path).to eq(shop_admin_stores_path)
    expect(page).not_to have_content(deleted_title)
  end
end
