require 'rails_helper'

feature "Notifications" do
  let!(:notifications) { FactoryGirl.create_list(:notification, 10, user: shop_admin_user) }

  before :each do
    visit_shop_admin_as_signed_in_user

    within '.nav-shop-admin-main' do
      click_link 'Уведомления'
    end
  end

  scenario "show notifications list" do
    expect(current_path).to eq(shop_admin_notifications_path)

    notifications.each do |notification|
      expect(page).to have_content(notification.subject)
      expect(page).to have_content(notification.message)
    end
  end

  scenario "destroy notification" do
    notification_id = notifications.first.id
    within "div[data-notification-id='#{notification_id}']" do
      click_link 'Удалить'
    end

    expect(current_path).to eq(shop_admin_notifications_path)
    expect(page).not_to have_css("div[data-notification-id='#{notification_id}']")
  end

  scenario "mark notification as read" do
    pending "should have ability to mark notification as read"
    raise "not implemented yet"
  end
end
