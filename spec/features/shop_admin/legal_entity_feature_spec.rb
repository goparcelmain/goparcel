require 'rails_helper'

feature "User legal entity" do
  before :each do
    visit_shop_admin_as_signed_in_user

    within '.profile-navigation' do
      click_link 'Реквизиты'
    end
  end

  scenario "show legal entity form" do
    expect(page).to have_css('h1', text: 'Реквизиты') 
  end

  scenario "change legal entity info" do
    fill_in 'Title', with: 'ООО "Магазин +100500"'
    fill_in 'Legal address', with: 'Пушкина, 700'
    fill_in 'Physical address', with: 'Ленина, 500'
    fill_in 'Ogrn', with: '1234123412342314'
    fill_in 'Inn', with: '12341234123412'
    fill_in 'Kpp', with: '123412341234'
    fill_in 'Okved', with: '12341234124'
    fill_in 'Okpo', with: '123412341234'
    fill_in 'Bik', with: '444'
    fill_in 'Bank name', with: 'ЗАО Банк'
    fill_in 'Correspondent account', with: '123123123123123'
    fill_in 'Checking account', with: '123123123123123'
    fill_in 'General manager', with: 'Петр'
    fill_in 'General accountant', with: 'Василий'
    fill_in 'Contact phone', with: '8(495)495-49-4'
    fill_in 'Contact email', with: 'shop@shop.net'
    fill_in 'Contract number', with: '123'

    click_button 'Сохранить'

    expect(current_path).to eq(edit_shop_admin_legal_entity_path)
    expect(page).to have_content('updated')
  end
end
