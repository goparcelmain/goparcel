# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :courier_service do
    title "MyString"
    contact_name "MyString"
    address "MyString"
    store_address "MyString"
    store_contact_name "MyString"
    phone "MyString"
    store_phone "MyString"
    email "MyString"
  end
end
