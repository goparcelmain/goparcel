# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :notification do
    subject { Faker::Name.title }
    message { Faker::Name.title  }
  end
end
