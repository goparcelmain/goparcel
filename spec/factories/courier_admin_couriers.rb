# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :courier_admin_courier, :class => 'CourierAdmin::Courier' do
    name "MyString"
    status_id 1
    passport "MyString"
    transport_type "MyString"
    phone "MyString"
    payments "MyString"
    string "MyString"
    orders_type "MyString"
  end
end
