# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :order_item do
    order_id 1
    shop_good_id "MyString"
    size "10x10x10"
    price "9.99"
    count 1
  end
end
