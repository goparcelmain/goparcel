# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :store do
    title { Faker::Company.name }
    contact_name { Faker::Name.name }
    phone { Faker::PhoneNumber.phone_number }
    postal_code "MyString"
    city "MyString"
    street "MyString"
    house "MyString"
    building "MyString"
    housing "MyString"
    room "MyString"
    porch 1
    floor 1
    description "MyText"
  end
end
