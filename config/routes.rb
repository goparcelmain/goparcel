Rails.application.routes.draw do
  root 'index#index'

  namespace :courier_admin do
    root 'index#index'
    resources :legal_entities
    resources :courier_services
    resources :couriers
    resources :orders do
      member do
        get :cancel
        get :courier
      end
    end
  end

  namespace :shop_admin do
    root 'index#index'
    
    devise_for :shop_users

    scope :profile do
      root 'profile#show', as: :profile
      resource :legal_entity, controller: :legal_entity, only: [:edit, :update]
      resources :stores
      resources :shops do
        resources :shop_delivery_values, except: [:index]
      end
    end

    scope :settings do
      root 'account_settings#edit', as: :settings
      resource :account_settings, only: [:update]
    end

    resources :notifications, only: [:index, :destroy]
    resources :reports, only: [:index]
    
    resources :orders, except: [:destroy] do
      resources :order_items, except: [:index, :show]
      member do
        get :publish
        get :confirm
        get :cancel
        get :courier
      end
    end

    resource :documents, only: [] do
      get :transmission_reception
      get :transmission_return
      get :cancel
      get :agent
    end
  end

  scope :courier_admin do
    devise_for :courier_users
  end

  namespace :gp_core do
    namespace :api do
      namespace :v1 do
        resources :orders
      end
    end
  end
end
