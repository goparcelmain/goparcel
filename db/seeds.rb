# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
shop_user = ShopUser.create(email: 'shop_user@example.com', password: 'password123')
courier_user = CourierUser.create(email: 'courier_user@example.com', password: 'password123')

shop = shop_user.shops.create(title: 'Test shop')
shop.stores.create(title: 'Test store')

courier_user.couriers.create(name: 'Test Courier')
