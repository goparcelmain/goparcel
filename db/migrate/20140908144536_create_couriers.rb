class CreateCouriers < ActiveRecord::Migration
  def change
    create_table :couriers do |t|
      t.integer :courier_user_id
      t.string :name
      t.integer :status_id
      t.string :passport
      t.string :transport_type
      t.string :phone
      t.string :payments
      t.string :string
      t.string :orders_type

      t.timestamps
    end
  end
end
