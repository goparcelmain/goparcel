class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :shop_user_id
      t.integer :shop_id
      t.string :shop_order_id
      t.string :shop_title
    
      t.integer :store_id
      t.string :store_address
      t.string :store_address_info

      t.integer :courier_user_id
      t.integer :courier_id
      t.string :courier_order_id
      t.string :courier_name
      t.string :courier_manager_phone

      t.integer :status_id, default: 0
      t.integer :return_reason
      t.integer :return_method


      t.string :customer_name
      t.string :customer_phone
      t.string :customer_second_phone
      t.string :customer_email
      t.string :delivery_address
      t.integer :floor
      t.string :intercom_code

      t.decimal :goods_estimated_price
      t.decimal :goods_price
      t.decimal :delivery_price
      t.decimal :shop_price
      t.decimal :goparcel_price
      t.decimal :return_delivery_price
      t.decimal :return_storage_fee

      t.string :payment_type
      t.text :goods
      t.text :comments

      t.datetime :assigned_at
      t.datetime :shipped_at
      t.datetime :delivered_at

      t.timestamps
    end
  end
end
