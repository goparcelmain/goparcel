class CreateOrderItems < ActiveRecord::Migration
  def change
    create_table :order_items do |t|
      t.integer :order_id
      t.string :shop_good_id
      t.string :size
      t.decimal :price
      t.decimal :estimated_price
      t.integer :count

      t.timestamps
    end
  end
end
