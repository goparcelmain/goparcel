class CreateLegalEntities < ActiveRecord::Migration
  def change
    create_table :legal_entities do |t|
      t.string :title
      t.string :legal_address
      t.string :physical_address
      t.string :ogrn
      t.string :inn
      t.string :kpp
      t.string :okved
      t.string :okpo
      t.string :bik
      t.string :bank_name
      t.string :correspondent_account
      t.string :checking_account
      t.string :general_manager
      t.string :general_accountant
      t.string :contact_phone
      t.string :contact_email
      t.string :contract_number
      t.integer :user_id
      t.string :user_type
      t.timestamps
    end
  end
end
