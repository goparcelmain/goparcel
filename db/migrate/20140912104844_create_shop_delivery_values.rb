class CreateShopDeliveryValues < ActiveRecord::Migration
  def change
    create_table :shop_delivery_values do |t|
      t.integer :shop_id
      t.decimal :less_than
      t.decimal :greater_than
      t.decimal :equal_to
      t.decimal :delivery_value

      t.timestamps
    end
  end
end
