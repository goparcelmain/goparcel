class CreateShops < ActiveRecord::Migration
  def change
    create_table :shops do |t|
      t.integer :shop_user_id
      t.string :title
      t.string :contact_name
      t.string :address
      t.string :phone
      t.string :email

      t.timestamps
    end
  end
end
