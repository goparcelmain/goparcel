class CreateCourierServices < ActiveRecord::Migration
  def change
    create_table :courier_services do |t|
      t.integer :courier_user_id
      t.string :title
      t.string :contact_name
      t.string :address
      t.string :store_address
      t.string :store_contact_name
      t.string :phone
      t.string :store_phone
      t.string :email

      t.timestamps
    end
  end
end
