class CreateStores < ActiveRecord::Migration
  def change
    create_table :stores do |t|
      t.integer :shop_id
      t.string :title
      t.string :contact_name
      t.string :postal_code
      t.string :city
      t.string :street
      t.string :house
      t.string :building
      t.string :housing
      t.string :room
      t.integer :porch
      t.integer :floor
      t.string :phone
      t.text :description
      t.boolean :as_default, default: false

      t.timestamps
    end
  end
end
