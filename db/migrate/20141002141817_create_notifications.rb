class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.string :subject
      t.text :message
      t.integer :user_id
      t.string :user_type
      t.integer :status, default: 0

      t.timestamps
    end
  end
end
