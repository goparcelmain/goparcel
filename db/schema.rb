# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141002141817) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "courier_services", force: true do |t|
    t.integer  "courier_user_id"
    t.string   "title"
    t.string   "contact_name"
    t.string   "address"
    t.string   "store_address"
    t.string   "store_contact_name"
    t.string   "phone"
    t.string   "store_phone"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "courier_users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "courier_users", ["email"], name: "index_courier_users_on_email", unique: true, using: :btree
  add_index "courier_users", ["reset_password_token"], name: "index_courier_users_on_reset_password_token", unique: true, using: :btree

  create_table "couriers", force: true do |t|
    t.integer  "courier_user_id"
    t.string   "name"
    t.integer  "status_id"
    t.string   "passport"
    t.string   "transport_type"
    t.string   "phone"
    t.string   "payments"
    t.string   "string"
    t.string   "orders_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "legal_entities", force: true do |t|
    t.string   "title"
    t.string   "legal_address"
    t.string   "physical_address"
    t.string   "ogrn"
    t.string   "inn"
    t.string   "kpp"
    t.string   "okved"
    t.string   "okpo"
    t.string   "bik"
    t.string   "bank_name"
    t.string   "correspondent_account"
    t.string   "checking_account"
    t.string   "general_manager"
    t.string   "general_accountant"
    t.string   "contact_phone"
    t.string   "contact_email"
    t.string   "contract_number"
    t.integer  "user_id"
    t.string   "user_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "notifications", force: true do |t|
    t.string   "subject"
    t.text     "message"
    t.integer  "user_id"
    t.string   "user_type"
    t.integer  "status",     default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "order_items", force: true do |t|
    t.integer  "order_id"
    t.string   "shop_good_id"
    t.string   "size"
    t.decimal  "price"
    t.decimal  "estimated_price"
    t.integer  "count"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "orders", force: true do |t|
    t.integer  "shop_user_id"
    t.integer  "shop_id"
    t.string   "shop_order_id"
    t.string   "shop_title"
    t.integer  "store_id"
    t.string   "store_address"
    t.string   "store_address_info"
    t.integer  "courier_user_id"
    t.integer  "courier_id"
    t.string   "courier_order_id"
    t.string   "courier_name"
    t.string   "courier_manager_phone"
    t.integer  "status_id",             default: 0
    t.integer  "return_reason"
    t.integer  "return_method"
    t.string   "customer_name"
    t.string   "customer_phone"
    t.string   "customer_second_phone"
    t.string   "customer_email"
    t.string   "delivery_address"
    t.integer  "floor"
    t.string   "intercom_code"
    t.decimal  "goods_estimated_price"
    t.decimal  "goods_price"
    t.decimal  "delivery_price"
    t.decimal  "shop_price"
    t.decimal  "goparcel_price"
    t.decimal  "return_delivery_price"
    t.decimal  "return_storage_fee"
    t.string   "payment_type"
    t.text     "goods"
    t.text     "comments"
    t.datetime "assigned_at"
    t.datetime "shipped_at"
    t.datetime "delivered_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "shop_delivery_values", force: true do |t|
    t.integer  "shop_id"
    t.decimal  "less_than"
    t.decimal  "greater_than"
    t.decimal  "equal_to"
    t.decimal  "delivery_value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "shop_users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
  end

  add_index "shop_users", ["email"], name: "index_shop_users_on_email", unique: true, using: :btree
  add_index "shop_users", ["reset_password_token"], name: "index_shop_users_on_reset_password_token", unique: true, using: :btree

  create_table "shops", force: true do |t|
    t.integer  "shop_user_id"
    t.string   "title"
    t.string   "contact_name"
    t.string   "address"
    t.string   "phone"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "stores", force: true do |t|
    t.integer  "shop_id"
    t.string   "title"
    t.string   "contact_name"
    t.string   "postal_code"
    t.string   "city"
    t.string   "street"
    t.string   "house"
    t.string   "building"
    t.string   "housing"
    t.string   "room"
    t.integer  "porch"
    t.integer  "floor"
    t.string   "phone"
    t.text     "description"
    t.boolean  "as_default",   default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
